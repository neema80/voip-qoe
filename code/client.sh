#!/bin/sh
echo "Running IPERF3 test on the target iperf-server"
iperf3 --client iperf-server --udp --bandwidth 64000 --interval .5 --time 5 --port 5001 --version4 --bidir | busybox tee -i iperf-data.txt
# busybox tail iperf-data.txt -n 5 | busybox head -n 3 > iperf1
# busybox head iperf1 -n 1 > iperf
# busybox tail iperf1 -n 1 >> iperf
# busybox rm iperf-data.txt iperf1
# busybox mv iperf iperf-public.txt
echo "IPERF3 Run OK"
echo "Running MTR test on the target iperf-server"
mtr --no-dns --report --report-cycles 10 iperf-server | busybox tee -i mtr.txt
# busybox tail mtr.txt -n 1 > mtr-public.txt
echo "MTR Run OK"
# busybox rm mtr.txt
