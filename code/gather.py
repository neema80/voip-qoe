#!/usr/bin/python3
from os import write
import subprocess
import os
import json
from colorama import init
from colorama import Fore, Back, Style
init()

########## Variable Definition
times = []
average_latency = 0.00
minimum_latency = 0.00
maximum_latency = 0.00
calculated_mos = 0.00
average_jitter = 0.00
minimum_jitter = 0.00
maximum_jitter = 0.00
packet_loss = 0

########## Constants Definion
# as the end to end devices uses codec G.722 this script uses computational time of 13
codec_impairment = 13
    # computational_time = codec delay
    # codec delay G.711 = 10 ms
    # codec processing delay G.722 = 13 ms
    # algorithmic delay for codec G.722 is 12.3125 defined in ANNEX B.
    # VoIP Codec Processing Delay in milliseconds (ms)
# latency_impact = 2

########## ENV check
print(Fore.YELLOW+'1. Environment Checking.......'+Fore.RESET)
print('Checking for folder /tmp/textfile')
if not os.path.exists('/tmp/textfile'):
	os.makedirs('/tmp/textfile')
	print(Fore.YELLOW+Style.BRIGHT+'WARN'+Fore.RESET+Style.NORMAL+': folder /tmp/textfile '+Fore.GREEN+Style.BRIGHT+'CREATED'+Fore.RESET+Style.NORMAL)
else:
	print(Fore.GREEN+Style.BRIGHT+'OK'+Fore.RESET+Style.NORMAL+': folder /tmp/textfile existed')
print('Environment Checking Finished.')
##### Calling outside Scripts
print(Fore.YELLOW+'2. PreRun Script Call.........'+Fore.RESET+Style.DIM)
subprocess.call(['sh', './client.sh'])
print(Fore.RESET+Style.NORMAL+'PreRun Script Call Finished.')

print(Fore.YELLOW+'3. Parsing Raw Data......'+Fore.RESET)
##### Parsing Raw Data
with open("iperf-public.txt","r") as raw:
    raw_data = raw.readlines()
raw_data = list(map(lambda s: s.strip('\n').strip(' ').split(), raw_data))
with open ("mtr-public.txt","r") as mtr_data:
        mtr = mtr_data.readlines()
mtr = list(map(lambda s: s.strip().split(), mtr))
print('Parsing Raw Data Finished.')

##### Functions Definitions
def MOS(codec_impairment, delay:float=0.00, packet_loss=0):
    delay_impairment = (0.024 * delay) 
    if (delay - 177.3) >= 0:
        delay_impairment = delay_impairment + (0.11 * (delay - 177.3))
    R = 93.2 - codec_impairment - delay_impairment - packet_loss
    if R < 0:
        mean_opinion_score = 1.0
    elif R > 100:
        mean_opinion_score = 4.5
    else:
        mean_opinion_score = round(1 + 0.035 * R + 0.000007 * R * (R-60) * (100 - R),3)    
    return mean_opinion_score

def extract_data(raw_data):
    times = []
    p_list = []
    average_jitter = 0.00
    packet_loss = 0
    for items in raw_data:
        times.append(float(items[8]))
        p_list.append(int(items[11].strip('(').strip(')').strip('%')))
    average_jitter = round(sum(times)/len(times),2)
    packet_loss = sum(p_list)//len(p_list)
    return average_jitter,packet_loss,times

##### Assign Values to Variables
average_jitter, packet_loss, times = extract_data(raw_data)
average_latency = float(mtr[0][5])
minimum_latency = float(mtr[0][6])
maximum_latency = float(mtr[0][7])
minimum_jitter = round(min(times),2)
maximum_jitter = round(max(times),2)
calculated_mos = MOS(codec_impairment,average_latency,packet_loss)

##### Output
print(Fore.GREEN+Style.BRIGHT+'==========================================================')
print('                      GATHERED FACTS')
print('Jitter Average       :',str(average_jitter))
print('Jitter Maximum       :',str(maximum_jitter))
print('Jitter Minimum       :',str(minimum_jitter))
print('Packet Loss          :',str(packet_loss))
print('Latency Average      :',str(average_latency))
print('Latency Minimum      :',str(minimum_latency))
print('LAtency Maximum      :',str(maximum_latency))
print('Mean Opinion Score   :',str(calculated_mos))
print(Fore.GREEN+'=========================================================='+Style.RESET_ALL)

prom_data = '''node_average_jitter {}
node_minimum_jitter {}
node_maximum_jitter {}
node_average_latency {}
node_minimum_latency {}
node_maximum_latency {}
node_mos {}
node_packet_loss {}
'''.format(average_jitter,minimum_jitter,maximum_jitter,average_latency,minimum_latency,maximum_latency,calculated_mos,packet_loss)
with open ('/tmp/textfile/test.prom','w') as prom_file:
   prom_file.writelines(prom_data)
