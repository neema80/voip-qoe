FROM alpine
LABEL Maintainer = "Nima Bahramzadeh <nima.bahramzadeh@gmail.com>"

RUN apk add iperf3

EXPOSE 5001
USER nobody
CMD ["iperf3", "--server", "--format", "k", "--port", "5001"]
