FROM alpine
LABEL Maintainer = "Nima Bahramzadeh <nima.bahramzadeh@gmail.com>"

RUN apk add iperf3 mtr python3


COPY code /code
RUN chmod +x /code/client.sh
RUN chmod +x /code/gather.py
# RUN chown nobody:nobody /code/
# RUN chown nobody:nobody /code/client.sh /code/gather.py
# USER nobody

CMD ["/code/client.sh"]
ENTRYPOINT ["sh"]